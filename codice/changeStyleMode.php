<?php
$style = $_COOKIE["style"] == "light" ? "dark" : "light";
setcookie("style", $style, time() + (86400 * 30), "/");
header('Location: ' . $_SERVER['HTTP_REFERER']);
?>