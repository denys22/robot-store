<form class="pb-3" action="#" method="POST" enctype="multipart/form-data">
    <div class="row col-12 col-sm-10 col-md-10 col-lg-8 ml-auto mr-auto py-3">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="input-name-robot">Nome Robot</label>
                <input type="text" class="form-control" id="input-name-robot" value="<?php echo $templateParams["currentRobot"]["nameRobot"] ?>" name="nameRobot">
            </div>
            <div class="form-group">
                <label for="input-price">Prezzo</label>
                <input type="number" min="1" step="any" class="form-control col-4" id="input-price" value="<?php echo $templateParams["currentRobot"]["price"] ?>" name="price">
            </div>
            <div class="form-group">
                <label for="input-image">Immagine</label>
                <input type="file" class="form-control-file" id="input-image" name="fileToUpload">
                <input type="hidden" class="form-control" name="oldImage" value="<?php echo $templateParams["currentRobot"]["imgRobot"] ?>">
            </div>
            <div class="form-group">
                <label for="categoria">Categoria</label>
                <select class="form-control" id="categoria" name="category">
                    <?php foreach($templateParams["categorie"] as $category): ?>
                        <option value="<?php echo current($category)?>" <?php if($templateParams["category"]==current($category)): ?>selected<?php endif; ?>><?php echo current($category)?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            
        </div>
        <div class="col-lg-6 form-group">
            <label for="input-description">Descrizione</label>
            <textarea class="form-control" id="input-description" rows="11" name="description"><?php echo $templateParams["currentRobot"]["description"] ?></textarea>
            <input type="hidden" class="form-control" name="idRobot" value="<?php echo $templateParams["currentRobot"]["idRobot"] ?>">
        </div>   
             
    </div>
    <div class="row justify-content-center">
        <input type="submit" class="btn btn-primary btn-lg col-5 col-sm-3 col-md-2 col-lg-2" name="submit" value="CONFERMA">
    </div>
</form>      
