
<script src="js/checkPassword.js"></script>
<form class="sign row" action="#" method="POST">
    <div class="col-lg-4 ml-auto mr-auto py-3">
    <h2 class="text-center">Crea un nuovo account</h2>
    <?php if(isset($templateParams["registerInfo"])): ?>
    <p><?php echo $templateParams["registerInfo"] ?></p>
    <?php endif; ?>
    <div class="form-group">
        <label for="input-email">Email</label>
        <input type="email" class="form-control" id="input-email" placeholder="Inserisci email" name="email">
    </div>
    <div class="form-group">
        <label for="input-password">Password</label>
        <input type="password" class="password-field form-control" id="input-password" placeholder="Inserisci password" name="password">
    </div>
    <div class="form-group">
        <label for="input-password-confirm">Conferma Password</label>
        <input type="password" class="password-field form-control" id="input-password-confirm" placeholder="Inserisci password" name="confirmPassword">
    </div>            
    <div class="row justify-content-center">
        <input id="registerbutton" type="submit" class="btn btn-primary btn-lg col-6" value="REGISTRATI">
    </div>
    </div>
</form>        