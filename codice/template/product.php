<div class="product-item col-lg-4 col-md-6 col-sm-12 d-flex justify-content-center pb-3">
    <a class="product-block" href="product-page.php?idRobot=<?php echo $robot["idRobot"] ?>">
        <figure>
        <img  class="img-fluid product-image" src=<?php echo IMG_DIR.$robot["imgRobot"] ?> alt="">
        <figcaption><?php echo $robot["nameRobot"] ?></figcaption>
        </figure>
        <span class="price"><?php echo $robot["price"]; ?>€</span>
    </a>
</div>