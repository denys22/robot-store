<nav class="navbar navbar-expand-lg navbar-default py-2 row row-no-gutters">
    <a class="navbar-brand" href="index.php"><img src=<?php echo "img/logo_robot_store.png" ?> alt="Logo Robot Store"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon pt-1">
            <em class="fa fa-bars" aria-hidden="true"></em>
        </span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a class="nav-link" href="index.php">Shop</a>
            </li>
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a class="nav-link" href="info.php">Info</a>
            </li>
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a id="darkk" class="nav-link" href="changeStyleMode.php">Change style</a>
            </li>
            <?php if(isAdminLoggedIn()): ?>
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a class="nav-link" href="insert-robot.php">Aggiungi robot</a>
            </li>
            <?php endif; ?>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <?php if(!isAdminLoggedIn()): ?>
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a class="nav-link" href="cart.php">Carrello
                <?php if(isset($templateParams["robotsInCart"]) && $templateParams["robotsInCart"] > 0): ?>
                <span class="badge badge-primary"><?php echo $templateParams["robotsInCart"] ?></span>
                <?php endif; ?>
                </a>
            </li>
            <?php endif; ?>
            <?php if(isUserLoggedIn() || isAdminLoggedIn()): ?>
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a class="nav-link" href="notification.php">
                Notifiche 
                <?php if(isset($templateParams["newNotification"]) && $templateParams["newNotification"] > 0): ?>
                <span class="badge badge-primary"><?php echo $templateParams["newNotification"] ?></span>
                <?php endif; ?>
                </a>
            </li>
            <li class="dropdown-divider"></li>
            <li class="nav-item">
                <a class="nav-link" href="utils/logout.php">Logout</a>
            </li>
            <?php else: ?>
            <li class="dropdown-divider"></li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="dropdownAccountButton" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Account</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownAccountButton">
                <a class="dropdown-item" href="login.php">Login</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="register.php">Register</a>
                </div>
            </li>
            <?php endif; ?>
            
        </ul>
    </div>
</nav>

