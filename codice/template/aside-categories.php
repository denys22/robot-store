<aside class="pb-3 row row-no-gutters">
      <ul class="nav nav-pills justify-content-center col-12">
          <li class="nav-item "><a class="nav-link <?php if($templateParams["currentCategoryId"] == 0){ echo "active";}?>" href="index.php">Home</a></li>
          <?php foreach($templateParams["categories"] as $category): ?>
          <li class="nav-item">
            <a class="nav-link <?php 
                  if($templateParams["currentCategoryId"] == $category["idCategory"]){
                    echo "active";
                  }?>"
                  href="index.php?category=<?php echo $category["idCategory"]; ?>">
                <?php echo $category["nameCategory"]; ?>
            </a>
          </li>
          <?php endforeach; ?>
      </ul>
</aside>