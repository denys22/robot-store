<?php foreach($templateParams["cart"] as $line): ?>
    <div class="row justify-content-center">
    <div class="cart-item row border col-11 col-sm-10 col-md-9 col-lg-6 pl-0">
        <div class="col-4 col-sm-3 col-md-3 col-lg-2 pt-2 pb-2 pr-0">
            <figure>
                <img class="img-fluid" src=<?php echo IMG_DIR.$line["imgRobot"]?> alt="">
            </figure>
        </div>
        <div class="cartInfo col-5 col-sm-4 col-md-4 col-lg-4 align-self-center">
            <span><?php echo $line["nameRobot"]?></span><br/>
            <span>Quantità: <?php echo $line["quantity"]?></span><br/>
            <span class="RemoveFromCart"><a href="cart.php?idRobotCartDelete=<?php echo $line["id"]?>">Rimuovi dal carrello</a></span>
        </div>
        <div class="cartPrice col-3 col-sm-3 col-md-2 col-lg-2 align-self-center offset-sm-2 offset-md-2 offset-lg-4">
            <span>Prezzo</span><br/>
            <span class="price"><?php echo $line["quantity"]*$line["price"]?>€</span>
        </div>
    </div>
</div>
<?php endforeach; ?>
<form id="makeOrderForm" class="row justify-content-center pt-4 pb-4" action="cart.php" method="POST">
    <input id="makeOrder" type="submit" class="btn btn-primary btn-lg col-8 col-sm-6 col-md-4 col-lg-3" name="confirmOrder" value="EFFETTUA L'ORDINE">
</form>

    