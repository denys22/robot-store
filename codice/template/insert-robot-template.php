<form class="pb-3" action="#" method="POST" enctype="multipart/form-data">
    <div class="row col-12 col-sm-10 col-md-10 col-lg-8 ml-auto mr-auto py-3">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="input-name-robot">Nome Robot</label>
                <input type="text" class="form-control" id="input-name-robot" placeholder="Inserisci nome" name="nameRobot">
            </div>
            <div class="form-group">
                <label for="input-price">Prezzo</label>
                <input type="number" min="1" step="any" class="form-control col-4" id="input-price" placeholder="Prezzo" name="price">
            </div>
            <div class="form-group">
                <label for="input-image">Immagine</label>
                <input type="file" class="form-control-file" id="input-image" name="fileToUpload">
            </div>
            <div class="form-group">
                <label for="categoria">Categoria</label>
                <select class="form-control" id="categoria" name="category">
                    <?php foreach($templateParams["categorie"] as $category): ?>
                        <option value="<?php echo current($category)?>"><?php echo current($category)?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            
        </div>
        <div class="col-lg-6 form-group">
            <label for="input-description">Descrizione</label>
            <textarea class="form-control" rows="11" id="input-description" placeholder="Inserisci descrizione Robot" name="description"></textarea>
        </div>        
    </div>
    <div class="row justify-content-center">
        <input type="submit" class="btn btn-primary btn-lg col-5 col-sm-3 col-md-2 col-lg-2" name="submit" value="CONFERMA">
    </div>
</form>      
