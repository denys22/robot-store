
<script src="js/checkPassword.js"></script>
<form class="sign row" action="#" method="POST">
    <div class="col-lg-4 ml-auto mr-auto">
    <h2 class="text-center">Login</h2>
    <?php if(isset($templateParams["loginInfo"])): ?>
    <p><?php echo $templateParams["loginInfo"] ?></p>
    <?php endif; ?>
    <div class="form-group">
        <label for="input-email">Email</label>
        <input type="email" class="form-control" id="input-email" aria-describedby="emailHelp" placeholder="Inserisci email" name="email">
    </div>
    <div class="form-group">
        <label for="input-password">Password</label>
        <input type="password" class="password-field form-control" id="input-password" placeholder="Inserisci password" name="password">
    </div>
    
    <div class="row justify-content-center">
        <input type="submit" class="btn btn-primary btn-lg col-6" name="submit" value="ACCEDI">
        <!--  <button id="loginbutton" type="submit" class="btn btn-primary btn-lg col-6">LOGIN</button> -->
    </div>
    <small id="emailHelp" class="form-text text-muted py-3"><a id="creaAccount" href="register.php">Crea un account</a></small>
    </div>
</form>        
    