<?php if(isset($templateParams["Notifications"])): ?>
    <?php foreach($templateParams["Notifications"] as $notif): ?>
        <div class="row justify-content-center">
            <div class="notification-item row border col-11 col-sm-10 col-md-9 col-lg-6 pl-0 mb-3">
                <div class="col-5 col-sm-5 col-md-5 col-lg-5 pt-2 pb-3">
                    <span><strong>Email: <?php echo $notif["emailSender"] ?></strong></span>
                </div>
                <div class="col-5 col-sm-5 col-md-5 col-lg-5 offset-2 offset-sm-2 offset-md-2 offset-lg-2 pt-2 pb-3 text-right">
                    <span><em><?php echo $notif["dateNotification"] ?></em></span>
                </div>
                <div class="col-10 col-sm-10 col-md-10 col-lg-8 offset-1 offset-sm-1 offset-md-1 offset-lg-2 text-center">
                    <?php echo $notif["message"] ?>
                </div>
                <div class="col-12 col-sm-8 col-md-8 col-lg-8 offset-sm-2 offset-md-2 offset-lg-2 pt-2 pb-3 text-center">
                    <form action="#" method="POST">
                    <?php if(isUserLoggedIn()): ?>
                        <input type="hidden" name="idUserNotification" value=<?php echo $notif["idNotification"] ?> >
                        <button type="submit" class="btn btn-primary btn-lg col-7 col-sm-6 col-md-6 col-lg-5 mb-2" name="confirmNotification">CONFERMA</button>
                    <?php elseif(isAdminLoggedIn()): ?> 
                        <input type="hidden" name="idSenderUser" value=<?php echo $notif["idSenderUser"] ?> >
                        <input type="hidden" name="idAdminNotification" value=<?php echo $notif["idNotification"] ?> >
                        <button type="submit" class="btn btn-primary btn-lg col-7 col-sm-5 col-md-5 col-lg-5 mb-2" name="confirmOrder">CONFERMA</button>
                        <button type="submit" class="btn btn-primary btn-lg col-7 col-sm-5 col-md-5 col-lg-5 mb-2" name="refuseOrder">RIFIUTA</button>
                    <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>