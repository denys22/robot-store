<footer class="page-footer row row-no-gutters font-small blue pt-4 mt-auto">
    <div class="container-fluid text-center text-md-center">
        <ul class="list-inline justify-content-center">
            <li class="list-inline-item"><a href="info.php">Privacy & Policy</a></li>
            <li class="list-inline-item"><a href="info.php">FAQ</a></li>
            <li class="list-inline-item"><a href="info.php">Contatti</a></li>
        </ul>
    </div>
</footer>