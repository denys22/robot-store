
<div class="row pt-4 px-md-3 px-lg-5">
    <div class="col-sm-12 col-md-3 col-lg-3">
        <figure>
            <img class="img-fluid" src=<?php echo IMG_DIR.$templateParams["currentRobot"]["imgRobot"] ?> alt="">
        </figure>
    </div>  
    <div class="col-sm-12 col-md-9 col-lg-9">
        <h1><?php echo $templateParams["currentRobot"]["nameRobot"] ?></h1>
        <span class="product-price"><?php echo $templateParams["currentRobot"]["price"] ?>€</span>
        <?php if(!isAdminLoggedIn()): ?>
        <form action="cart.php" method="POST">
            <div class="row col-12 pt-2 d-flex flex-row">
                <div class="col-3 col-sm-2 col-md-2 col-lg-2 nopadding-left">
                        <label for="quantity">Quantità</label>
                        <input type="number" min="1" class="form-control" id="quantity" name="quantity" value="1">
                    <input type="hidden" class="form-control" name="idRobot" value="<?php echo $templateParams["currentRobot"]["idRobot"] ?>">
                </div>
                <div class="col-9 col-sm-10 col-md-10 col-lg-10 align-self-end">
                    <button type="submit" id="buyButton" class="btn btn-primary btn-lg">AGGIUNGI AL CARRELLO</button>
                </div> 
            </div>
        </form>
        <?php endif; ?>
        <?php if(isAdminLoggedIn()): ?>
        <form action="modify-robot.php" method="POST">
            <div class="row col-sm-12 col-md-12 col-lg-12 pt-2 d-flex flex-row justify-content-left">
                <input type="hidden" class="form-control" name="idRobot" value="<?php echo $templateParams["currentRobot"]["idRobot"] ?>">
                <div class="mr-auto">
                    <button type="submit" name="modify" class="btn btn-primary btn-lg mb-1">MODIFICA PRODOTTO</button>
                    <button type="submit" name="delete" class="btn btn-primary btn-lg mb-1">ELIMINA PRODOTTO</button>
                </div> 
            </div>
        </form>
        <?php endif; ?>
        <div class="row col-lg-9 pt-3">
            <p><?php echo $templateParams["currentRobot"]["description"] ?></p>
        </div>
    </div>
</div>