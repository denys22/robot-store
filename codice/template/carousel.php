<div class="row justify-content-center pb-3">
    <div id="carouselExampleIndicators" class="carousel slide col-md-8 col-sm-12" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src=<?php echo "img/b1.jpg" ?> alt="Future Robots - Read More" aria-describedby="b1-desc">
            <span class="sr-only" id="b1-desc">Lorem impsum dolor sit amet, consecletur adipiscing elit, sed do eiusmod tempor</span>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src=<?php echo "img/b2.jpg" ?> alt="New Technologies - Read More" aria-describedby="b2-desc">
            <span class="sr-only" id="b2-desc">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commoda</span>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src=<?php echo "img/b3.jpg" ?> alt="Home Robots - Read More" aria-describedby="b3-desc">
            <span class="sr-only" id="b3-desc">Duis aute irure  dolor in reprehenderit in valuptate velit esse cilllum dolore eu  fugia nulla pariatur. Excepteur sint</span>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Precedente</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Prossimo</span>
    </a>
    </div>
</div>