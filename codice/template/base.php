<?php if(!isset($_COOKIE["style"])){
  setcookie("style", "light", time() + (86400 * 30), "/");
} ?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"> <!-- Setting the viewport to make website look good on all devices: -->
    <meta name="author" content="Denys Grushchak, Francesco Ballarini">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <?php if(!isset($_COOKIE["style"]) || $_COOKIE["style"] == "light"): ?>
    <link rel="stylesheet" href="css/style.css" >
    <?php else: ?>
    <link rel="stylesheet" href="css/darkStyle.css" >
    <?php endif;?>
    <!-- Title -->
    <?php if(isset($templateParams["title"])): ?>
    <title><?php echo $templateParams["title"] ?></title>
    <?php endif;?>
</head>
<body>
  <div class="container-fluid d-flex flex-column min-vh-100">
    <?php 
        require($templateParams["navbar"]); 
    ?>
    <?php if(isset( $templateParams["header"])): ?>
    <header class="py-1 row pt-3 pb-3">
      <div class="col-12">
        <h1 class="text-center"><?php echo $templateParams["header"]; ?></h1>
      </div>
    </header> 
    <?php endif; ?>
    <?php 
    if(isset($templateParams["aside"])){
      require($templateParams["aside"]);
    }?>
    <?php
    if(isset($templateParams["carousel"])){
      require($templateParams["carousel"]);
    }?>
    <main>
      <?php require($templateParams["main"]); ?>
    </main>
    <?php
        require($templateParams["footer"])
    ?>
  </div>
  
<?php
  require($templateParams["bootstrapScripts"]);
?>
</body>
</html>