const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;

$(function(){
    const username = $("input#input-email");
    const passwords = $("input.password-field");
    $("main form.sign").submit(function(e){
        const username_value = username.val();
        
        username.next().remove();
        if(username_value.length < 5){
            username.parent().append("<p>Lo username deve essere almeno 5 caratteri</p>");
            e.preventDefault();
        }
        
        passwords.last().next().remove();
        $.each(passwords, function () { 
            const password_value = $(this).val();
            if(password_value < 5 || !pattern.test(password_value)){
                passwords.last().parent().append("<p> La password deve essere almeno 5 caratteri e contenere almeno un numero e una lettera</p>");
                e.preventDefault();
                return false;
            }
        });
        
    });
})