<?php
require_once("bootstrap.php");

if(isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["confirmPassword"])){
    if($_POST["password"] == $_POST["confirmPassword"]){
        if(!isEmailValid($_POST["email"])){
            $templateParams["registerInfo"] = "Email non valido";
        }else if(!isPasswordValid($_POST["password"])){
            $templateParams["registerInfo"] = "Password non valido";
        }else if(empty($dbh->getUser($_POST["email"])) && empty($dbh->getAdmin($_POST["email"]))){
            $_SESSION["loginInfo"] = "Sei reggistrato con sucesso!";
            $dbh->registerUser($_POST["email"],$_POST["password"]);
            header("Location: ".MYPATH."login.php");
        }else{
            $templateParams["registerInfo"] = "Email già registrato";
        }
    }else{
        $templateParams["registerInfo"] = "Sono state inserite password differenti";
    }
}

$templateParams["main"] = "register-template.php";
$templateParams["header"] = "Account";
$templateParams["title"] = "Robot Store - registrazione";
require("template/base.php");
?>