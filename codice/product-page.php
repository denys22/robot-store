<?php
require_once("bootstrap.php");

$templateParams["main"] = "template/product-page-template.php";
if(isset($_GET["idRobot"])){
    $result = $dbh->getRobot($_GET["idRobot"]);
    if(!empty($result)){
        $templateParams["currentRobot"] = $result[0];
    }else{
        $templateParams["main"] = "utils/error.html";
    }
    
}else{
    $templateParams["main"] = "utils/error.html";
}
if(isset($_POST["quantity"])){
    $_SESSION["cart"]=array($result[0] => $_POST["quantity"]);
}
$templateParams["title"] = $templateParams["currentRobot"]["nameRobot"];
require("template/base.php");
?>