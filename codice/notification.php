<?php
require_once("bootstrap.php");
require_once("utils/notificationFunctions.php");


if(isUserLoggedIn()){
    if(isset($_POST["confirmNotification"]) && isset($_POST["idUserNotification"])){
        $dbh->deleteUserNotification($_POST["idUserNotification"]);
    }
    $templateParams["Notifications"] = $dbh->readUserNotifications($_SESSION["idUser"]);
    $dbh->updateTimeOfAccessToUserNotification($_SESSION["idUser"]);

}elseif(isAdminLoggedIn()){
    if(isset($_POST["idAdminNotification"]) && isset($_POST["idSenderUser"])){
        $messageHtml= "";
        if(isset($_POST["confirmOrder"])){
            $messageHtml = createShortNotification("Il tuo ordine è stato confermato");
        }elseif(isset($_POST["refuseOrder"])){
            $messageHtml = createShortNotification("Il tuo ordine è stato rifiutato");
        }
        $dbh->sendNotificationToUser($messageHtml,$_POST["idSenderUser"],$_SESSION["idAdmin"]);
        $dbh->deleteAdminNotification($_POST["idAdminNotification"]);
    }
    
    $templateParams["Notifications"] = $dbh->readAdminNotifications();
    $dbh->updateTimeOfAccessToAdminNotification($_SESSION["idAdmin"]);
}else{
    header("Location: ".MYPATH."login.php");;
}

$templateParams["main"] = "notification-template.php";
$templateParams["header"] = "notifiche";
$templateParams["title"] = "Robot store - notifiche";
require("template/base.php");
?>