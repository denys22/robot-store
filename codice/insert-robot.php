<?php
require_once("bootstrap.php");

if(!isAdminLoggedIn()){
    header("Location: ".MYPATH."login.php");
}
if(isset($_POST["nameRobot"]) && isset($_POST["price"]) && isset($_POST["description"]) && isset($_POST["category"])){
    $imgName=uploadImage("default.jpg");

    $categoryId = current(current($dbh->getCategoryId($_POST["category"])));
    $dbh->insertRobot($_POST["nameRobot"],round($_POST["price"],2),$_POST["description"],$categoryId, $imgName);
    $idRobots = $dbh->getRobotByData($_POST["nameRobot"],round($_POST["price"],2),$_POST["description"]);
    $idRobot = current(current($idRobots));
    header("Location: ".MYPATH."product-page.php?idRobot=$idRobot");
}
$templateParams["main"] = "insert-robot-template.php";
$templateParams["header"] = "inserisci robot";
$templateParams["title"] = "Robot store - inserisci un nuovo robot";
$templateParams["categorie"] = $dbh->getCategoriesNames();
require("template/base.php");
?>