<?php
require_once("bootstrap.php");
require_once("utils/notificationFunctions.php");


if(!isUserLoggedIn()){
    header("Location: ".MYPATH."login.php");
}
if (isset($_GET['idRobotCartDelete'])) {
    $dbh->deleteFromCart($_GET['idRobotCartDelete']) ;
}
if(isset($_POST["quantity"]) && isset($_POST["idRobot"]) && isset($_SESSION["idUser"])){
    $dbh->insertRobotInCart($_POST["idRobot"], $_SESSION["idUser"], $_POST["quantity"]);
}

if(isset($_POST["confirmOrder"]) && isset($_SESSION["idUser"])){ // manage order
    $messageHtml = createOrderNotification();
    if(!empty($messageHtml)){
        $dbh->sendNotificationToAdmins($messageHtml,$_SESSION["idUser"]);
        $dbh->clearCart($_SESSION["idUser"]);
        header("Location: ".MYPATH."index.php");
    }
}


calculateRobotsInCart($_SESSION["idUser"]);
$templateParams["robotsInCart"] = howManyRobotsInCart();
$templateParams["cart"] = $dbh->getRobotsInCart($_SESSION["idUser"]);
$templateParams["main"] = "cart-template.php";
$templateParams["header"] = "carrello";
$templateParams["title"] = "Robot store - carrello";
require("template/base.php");
?>