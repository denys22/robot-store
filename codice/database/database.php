<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }
    public function getCategories(){
        $stmt = $this->db->prepare("SELECT * FROM category");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoriesNames(){
        $stmt = $this->db->prepare("SELECT nameCategory FROM category");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoryId($nameCategory){
        $stmt = $this->db->prepare("SELECT idCategory FROM category WHERE nameCategory = ?");
        $stmt->bind_param('s',$nameCategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRobot($id){
        $stmt = $this->db->prepare("SELECT * FROM robot where idRobot = ?");
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRobotByData($nameRobot, $price, $description){
        $stmt = $this->db->prepare("SELECT idRobot FROM robot where nameRobot = ? AND price = ? AND description = ?");
        $stmt->bind_param('sds',$nameRobot, $price, $description);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllRobots(){
        $stmt = $this->db->prepare("SELECT * FROM robot");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getRobots($idCategory){
        $stmt = $this->db->prepare("SELECT * FROM robot where idCategory = ?");
        $stmt->bind_param('i',$idCategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function registerUser($email, $password){
        $hashed_password = password_hash($password,PASSWORD_DEFAULT);
        $stmt = $this->db->prepare("INSERT INTO user(email,password,lastOpenNotification) VALUES (?, ?, now())");
        $stmt->bind_param('ss', $email, $hashed_password);
        $stmt->execute();

    }
    // public function registerAdmin($email, $password){
    //     $hashed_password = password_hash($password,PASSWORD_DEFAULT);
    //     $stmt = $this->db->prepare("INSERT INTO admin(email, password, name) VALUES (?, ?, ?)");
    //     $name ="Francesco"; // TO DO NOME
    //     $stmt->bind_param('sss', $email, $hashed_password, $name);
    //     $stmt->execute();

    // }

    public function getUser($email){
        $stmt = $this->db->prepare("SELECT idUser, password FROM user WHERE email = ?");
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getAdmin($email){
        $stmt = $this->db->prepare("SELECT idAdmin, password, name FROM admin WHERE email = ?");
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function insertRobotInCart($idRobot, $idUser, $quantity){
        if (!empty($this->getRobot($idRobot)[0]) && ($quantity>0)){
            $stmt = $this->db->prepare("SELECT quantity FROM robotincart WHERE idRobot = ? AND idUser = ?");
            $stmt->bind_param('ii', $idRobot, $idUser);
            $stmt->execute();
            $result = $stmt->get_result();
            $result = $result->fetch_all(MYSQLI_ASSOC);
            if(empty($result[0])){
                $stmt = $this->db->prepare("INSERT INTO robotincart(idRobot,quantity,idUser) VALUES (?, ?, ?)");
                $stmt->bind_param('iii', $idRobot, $quantity, $idUser);
                $stmt->execute();
            }else{
                $stmt = $this->db->prepare("UPDATE robotincart SET quantity=? WHERE idRobot = ? AND idUser = ?");
                $newQuantity = $result[0]["quantity"] + $quantity;
                $stmt->bind_param('iii',$newQuantity, $idRobot, $idUser);
                $stmt->execute();
            }
        }
    }

    public function getRobotsInCart($idUser){
        $query = <<<SQL
        SELECT robotincart.idRobot, quantity, nameRobot, price, imgRobot, robotincart.id
        FROM robotincart, robot 
        WHERE robot.idRobot = robotincart.idRobot 
        AND robotincart.idUser = ?
SQL;
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteFromCart($idRobotInCart){
        $stmt = $this->db->prepare("DELETE FROM robotincart WHERE id = ?");
        $stmt->bind_param('i', $idRobotInCart);
        $stmt->execute();
    }

    public function howManyRobotsInCart($idUser){
        $stmt = $this->db->prepare("SELECT sum(quantity) as totalQuantity FROM robotincart WHERE idUser = ?");
        $stmt->bind_param('s',$idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0];
    }
    
    public function insertRobot($nameRobot, $price, $description, $idCategory, $imgRobot){
        if (empty($this->findDuplicateRobot($nameRobot, $price, $description))){
            $stmt = $this->db->prepare("INSERT INTO robot(nameRobot,price,description,idCategory,imgRobot) VALUES (?, ?, ?, ?, ?)");
            $stmt->bind_param('sdsis', $nameRobot, $price, $description, $idCategory, $imgRobot);
            $stmt->execute();
        }
    }

    public function findDuplicateRobot($nameRobot, $price, $description){
        $stmt = $this->db->prepare("SELECT * FROM robot WHERE nameRobot = ? AND price = ? AND description = ?");
        $stmt->bind_param('sds',$nameRobot, $price, $description);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteRobot($idRobot){
        $stmt = $this->db->prepare("DELETE FROM robot WHERE idRobot = ?");
        $stmt->bind_param('i', $idRobot);
        $stmt->execute();

        $stmt = $this->db->prepare("DELETE FROM robotincart WHERE idRobot = ?");
        $stmt->bind_param('i', $idRobot);
        $stmt->execute();
    }

    public function getCategoryName($idCategory){
        $stmt = $this->db->prepare("SELECT nameCategory FROM category WHERE idCategory = ?");
        $stmt->bind_param('i',$idCategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function modifyRobot($nameRobot, $price, $description, $idCategory, $imgRobot,$idRobot){
        $stmt = $this->db->prepare("UPDATE robot SET robot.nameRobot = ?, robot.price = ?, robot.description = ?, robot.idCategory = ?, robot.imgRobot = ? WHERE robot.idRobot = ?");
        $stmt->bind_param('sdsisi', $nameRobot, $price, $description, $idCategory, $imgRobot, $idRobot);
        $stmt->execute();
    }

    public function sendNotificationToAdmins($messageHtml,$idUser){
        $stmt = $this->db->prepare("INSERT INTO admin_notification(dateNotification, message, idSenderUser) VALUES(now(),?,?)");
        $stmt->bind_param('si',$messageHtml,$idUser);
        $stmt->execute();

    }

    public function sendNotificationToUser($messageHtml,$idUser,$idAdmin){
        $stmt = $this->db->prepare("INSERT INTO user_notification(dateNotification, message, idUser, idSender) VALUES(now(),?,?,?)");
        $stmt->bind_param('sii',$messageHtml,$idUser, $idAdmin);
        $stmt->execute();

    }

    public function readAdminNotifications(){
        $query=<<<SQL
        SELECT idNotification, email as emailSender, dateNotification, message, an.idSenderUser 
        FROM admin_notification an, user 
        WHERE an.idSenderUser = user.idUser
SQL;
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function readUserNotifications($idUser){
        $query=<<<SQL
        SELECT idNotification, email as emailSender, dateNotification, message
        FROM user_notification un, admin 
        WHERE un.idUser = ?
        AND un.idSender = admin.idAdmin
SQL;
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function clearCart($idUser){
        $stmt = $this->db->prepare("DELETE FROM robotincart WHERE idUser = ?");
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
    }

    public function deleteUserNotification($idNotification){
        $stmt = $this->db->prepare("DELETE FROM user_notification WHERE idNotification = ?");
        $stmt->bind_param('i', $idNotification);
        $stmt->execute();
    }
    public function deleteAdminNotification($idNotification){
        $stmt = $this->db->prepare("DELETE FROM admin_notification WHERE idNotification = ?");
        $stmt->bind_param('i', $idNotification);
        $stmt->execute();
    }

    public function updateTimeOfAccessToAdminNotification($idAdmin){
        $stmt = $this->db->prepare("UPDATE admin SET lastOpenNotification = now() WHERE idAdmin = ?");
        $stmt->bind_param('i',$idAdmin);
        $stmt->execute();

    }

    public function updateTimeOfAccessToUserNotification($idUser){
        $stmt = $this->db->prepare("UPDATE user SET lastOpenNotification = now() WHERE idUser = ?");
        $stmt->bind_param('i',$idUser);
        $stmt->execute();

    }

    public function countNewAdminNotification($idAdmin){
        $query=<<<SQL
        SELECT count(idNotification) as newNotification 
        FROM admin, admin_notification 
        WHERE admin.idAdmin = ? 
        AND lastOpenNotification < dateNotification
SQL;
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idAdmin);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0]["newNotification"];
    }

    public function countNewUserNotification($idUser){
        $query=<<<SQL
        SELECT count(idNotification) as newNotification 
        FROM user, user_notification 
        WHERE user.idUser = ? 
        AND lastOpenNotification < dateNotification
SQL;
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0]["newNotification"];
    }

    public function getRobotByImage($imgRobot){
        $stmt = $this->db->prepare("SELECT * FROM robot WHERE imgRobot = ?");
        $stmt->bind_param('s',$imgRobot);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getImageRobot($idRobot){
        $stmt = $this->db->prepare("SELECT imgRobot FROM robot WHERE idRobot = ?");
        $stmt->bind_param('i',$idRobot);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
?>