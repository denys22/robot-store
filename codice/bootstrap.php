<?php
session_start();

require_once("database/database.php");
$dbh = new DatabaseHelper("localhost", "root", "", "robot-store", 3306);

$templateParams["bootstrapScripts"] = "utils/bootstrapScripts.html";
$templateParams["navbar"] = "navbar.php";
$templateParams["footer"] = "footer.php";


require_once("utils/loginFunctions.php");
require_once("utils/cartFunctions.php");
require_once("utils/imageFunctions.php");
if(isUserLoggedIn()){
    $templateParams["robotsInCart"] = howManyRobotsInCart();
    $templateParams["newNotification"] = $dbh->countNewUserNotification($_SESSION["idUser"]);
}elseif(isAdminLoggedIn()){
    $templateParams["newNotification"] = $dbh->countNewAdminNotification($_SESSION["idAdmin"]);
}

define("IMG_DIR", "img/upload/");
define("MYPATH", "/robot-store/codice/");
?>