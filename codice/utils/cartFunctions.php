<?php

function howManyRobotsInCart(){
    if(isset($_SESSION["robotsInCart"])){
        return $_SESSION["robotsInCart"];
    }
    return 0;
}
function calculateRobotsInCart($idUser){
    global $dbh;
    $_SESSION["robotsInCart"] = $dbh->howManyRobotsInCart($idUser)["totalQuantity"];
}

?>