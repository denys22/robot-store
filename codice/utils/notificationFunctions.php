<?php

function createOrderNotification(){
    global $dbh;
    $robots = $dbh->getRobotsInCart($_SESSION["idUser"]);
    if(empty($robots)){
        return null;
    }
    $message = "<p>È stato effetuato un ordine che contiene seguenti robot:</p>";
    $message = $message."<ul>";
    foreach($robots as $robot){
        $message = $message ."<li>". $robot["nameRobot"] . " con quantità: " .   $robot["quantity"] . "</li>";
    }
    $message = $message ."</ul>";
    return $message;

}

function createShortNotification($text){
    return "<p>". $text . "</p>";
}

?>