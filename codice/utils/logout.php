<?php
require_once("../bootstrap.php");

if(isset($_SESSION["idUser"]) || isset($_SESSION["idAdmin"])){
    session_destroy();
    header("Location: ".MYPATH."index.php");

}else{
    echo "Error: you are not logged in";
}

?>