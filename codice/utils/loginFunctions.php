<?php

function isUserLoggedIn(){
    return !empty($_SESSION['idUser']);
}

function isAdminLoggedIn(){
    return !empty($_SESSION['idAdmin']);
}

function registerLoggedUser($user){
    $_SESSION["idUser"] = $user["idUser"];
    calculateRobotsInCart($user["idUser"]);
}

function registerLoggedAdmin($admin){
    $_SESSION["idAdmin"] = $admin["idAdmin"];
    $_SESSION["adminName"] = $admin["name"];
}

function checkLogin($email, $password){
    global $dbh;
    if(isEmailValid($email) && isPasswordValid($password)){
        $result = $dbh->getUser($email);
        if(!empty($result)){
            $user = $result[0];
            if(password_verify($password, $user["password"])){
                registerLoggedUser($user);
                return "Sei loggato con successo!";
            }
        }else{
            $result = $dbh->getAdmin($email,$password);
            if(!empty($result)){
                $admin = $result[0];
                if(password_verify($password, $admin["password"])){
                    registerLoggedAdmin($admin);
                    return "Sei loggato con successo!";
                }

            }
        }
    }
    
    return "Email o password errato";
}    

function isEmailValid($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function isPasswordValid($password){
    $pattern = "#.*^(?=.{5,20})(?=.*[a-z])(?=.*[0-9]).*$#";
    return preg_match($pattern, $password);
}
?>