<?php
require_once("bootstrap.php");
if(isset($_SESSION["loginInfo"])){
    $templateParams["loginInfo"] = $_SESSION["loginInfo"];
    unset($_SESSION["loginInfo"]);
}

if(isset($_POST["email"]) && isset($_POST["password"]) ){
    $templateParams["loginInfo"] = checkLogin($_POST["email"],$_POST["password"]);
}
 

if(isUserLoggedIn()){
    header("Location: ".MYPATH."index.php");
}else if(isAdminLoggedIn()){
    $templateParams["header"] = "Ciao ".$_SESSION["adminName"];
    $templateParams["main"] = "utils/admin-page.html";
    $templateParams["newNotification"] = $dbh->countNewAdminNotification($_SESSION["idAdmin"]);
}else{
    $templateParams["header"] = "Account";
    $templateParams["main"] = "login-template.php";
}
$templateParams["title"] = "Robot Store - Login";
require("template/base.php");
?>