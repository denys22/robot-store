
<?php
require_once("bootstrap.php");

if(isset($_POST["delete"]) && isset($_POST["idRobot"])){
    $imgRobot=current(current($dbh->getImageRobot($_POST["idRobot"])));
    $dbh->deleteRobot($_POST["idRobot"]);
    if (empty($dbh->getRobotByImage($imgRobot)) && ($imgRobot!="default.jpg")){
        unlink(IMG_DIR.$imgRobot);
    }
    header("Location: ".MYPATH."index.php");
}
if(isset($_POST["nameRobot"]) && isset($_POST["price"]) && isset($_POST["description"]) && isset($_POST["category"])){
    $imgName=uploadImage($_POST["oldImage"]);
    
    $categoryId = current(current($dbh->getCategoryId($_POST["category"])));
    $idRobot = $_POST["idRobot"];
    $dbh->modifyRobot($_POST["nameRobot"],round($_POST["price"],2),$_POST["description"],$categoryId,$imgName, $idRobot);
    if (empty($dbh->getRobotByImage($_POST["oldImage"])) && ($_POST["oldImage"]!="default.jpg")){
        unlink(IMG_DIR.$_POST["oldImage"]);
    }
    header("Location: ".MYPATH."product-page.php?idRobot=$idRobot");
}else if(isset($_POST["modify"]) && isset($_POST["idRobot"])){
    $templateParams["main"] = "template/modify-robot-template.php";
    $templateParams["header"] = "modifica robot";
    $result = $dbh->getRobot($_POST["idRobot"]);
    if(!empty($result)){
        $templateParams["currentRobot"] = $result[0];
    }
    $templateParams["categorie"] = $dbh->getCategoriesNames();
    $templateParams["category"] = current(current($dbh->getCategoryName($templateParams["currentRobot"]["idCategory"])));
}else{
    header("Location: ".MYPATH."index.php");
}
$templateParams["title"] = "Robot store - modifica robot";
require("template/base.php");
?>