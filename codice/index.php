<?php
require_once("bootstrap.php");

$templateParams["navbar"] = "navbar.php";
$templateParams["aside"] = "template/aside-categories.php";
$templateParams["main"] = "template/productList.php";
$templateParams["footer"] = "footer.php";
$templateParams["product"] = "product.php";
$templateParams["title"] = "Robot store - miglior negozio che vende dei robot. Pagina principale";
$templateParams["categories"] = $dbh->getCategories();
$templateParams["header"] = "Robot Store";
if(!isset($_GET["category"])){
    $templateParams["carousel"] = "carousel.php";
}

$templateParams["currentCategoryId"] = isset($_GET["category"]) ? $_GET["category"] : 0;
$templateParams["robots"] = $templateParams["currentCategoryId"] == 0 ? $dbh->getAllRobots() : $dbh->getRobots($templateParams["currentCategoryId"]);


require("template/base.php");
?>