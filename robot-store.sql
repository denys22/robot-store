-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2020 at 05:32 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `robot-store`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idAdmin` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `lastOpenNotification` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idAdmin`, `email`, `password`, `name`, `lastOpenNotification`) VALUES
(3, 'denys@admin.it', '$2y$10$0y14Yw6sAYYGYdHt14fWmOdkz89qjCT0XKHMIHDqITm2iWaUvWb1G', 'Denys', '2020-12-30 14:45:11'),
(4, 'francesco@admin.it', '$2y$10$cCOqtzWV9qGnmaYCEct2kuRumuh3k2Ahd3jVK4RIFixqgPiBFqioO', 'Francesco', '2020-12-22 12:47:17');

-- --------------------------------------------------------

--
-- Table structure for table `admin_notification`
--

CREATE TABLE `admin_notification` (
  `idNotification` int(11) NOT NULL,
  `dateNotification` datetime NOT NULL,
  `message` text NOT NULL,
  `idSenderUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `idCategory` int(11) NOT NULL,
  `nameCategory` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`idCategory`, `nameCategory`) VALUES
(1, 'Domestici'),
(2, 'Bipedi'),
(3, 'Specializzati'),
(4, 'Intelligenti'),
(5, 'Militari');

-- --------------------------------------------------------

--
-- Table structure for table `robot`
--

CREATE TABLE `robot` (
  `idRobot` int(11) NOT NULL,
  `nameRobot` text NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `idCategory` int(11) NOT NULL,
  `imgRobot` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `robot`
--

INSERT INTO `robot` (`idRobot`, `nameRobot`, `description`, `price`, `idCategory`, `imgRobot`) VALUES
(8, 'Robot giallo', 'Robotino esploratore da costruire, che usa l\'IA e i sensori infrarossi per esplorare l\'ambiente e individuare gli ostacoli. In modalità Seguimi, il robotino segue qualunque oggetto o persona si avvicini. In modalità Esplora osserva l\'ambiente circostante evitando gli ostacoli. Ha sei gambe che gli permettono di ruotare a 360 gradi e aggirare gli ostacoli. Alto cm 17. Da 8 anni.', '300.00', 2, 'robot_giallo.jpg'),
(11, 'Butter robot', 'Rispetto al solito standard scientifico di Rick, il robot del burro sembra un gadget relativamente semplice: il suo scopo è, semplicemente, quello di passare il burro. Quando Rick attiva la sua ultima creazione, l’intelligenza artificiale chiede quale sia il suo scopo, rivelando un livello di coscienza superiore a quello necessario per una macchina destinata a svolgere una funzione puramente meccanica. Come molti esseri di intelligenza superiore, il robot del burro è costernato nell’apprendere che esiste per ragioni puramente banali.', '109.00', 1, 'Butter_Robot_Picture.png'),
(13, 'R2-D2', 'È un droide specializzato nell\'interfacciarsi con ogni sorta di computer, attivare ogni macchinario come ascensori, ed abile nelle riparazioni. Proveniente dal pianeta Naboo, è dotato di una buona dose di umorismo. È bravo a prendere certe iniziative impiegando metodi a volte poco ortodossi per aiutare i suoi amici. È l\'inseparabile compagno del droide protocollare C-3PO \r\n\r\nOwen Lars ipotizza che il suo particolare \"carattere\" sia dovuto all\'assenza di cancellazioni di memoria regolari, il che in un droide può provocare instabilità. In effetti la memoria di R2-D2 non viene mai cancellata dalla fuga da Naboo in poi (cioè 36 anni prima della morte di Dart Fener).', '7800.00', 4, 'C7E3tSjU0AQEKOr.jpg'),
(14, 'Wall-e', 'Wall-e è simile a un cubo e ha delle braccia meccaniche e un dispositivo che gli consente di compattare i rifiuti. All\'inizio era un robot senz\'anima e freddo ma col passare degli anni sviluppa una personalità, grazie anche agli oggetti che trova e conserva, soprattutto grazie alla cassetta del film Hello, Dolly! che gli fa conoscere l\'amore e gli fa sognare di trovare una compagna. Si affeziona a EVE fin da subito anche se non viene considerato, sviluppa un profondo amore per la sonda e farà di tutto per lei. Alla fine, gravemente danneggiato e poi riparato perde la memoria, che gli ritorna dopo il bacio di EVE.', '699.00', 3, 'wall-e.jpg'),
(15, 'Spot', 'Il robot Spot è un robot mobile con gambe che può muoversi su diversi terreni. Spot utilizza più sensori e 3 motori in ogni gamba per passare da ambienti interni ad ambienti esterni, mantenere l\'equilibrio e mettersi in posizione. Spot è un robot mobile dall\'elevato grado di autonomia in termini di locomozione. Il comportamento del robot durante il movimento può variare rispetto alle traiettorie o ai movimenti pianificati. Il robot esegue operazioni di locomozione e spostamento in una vasta gamma di ambienti interni ed esterni.\r\n', '13000.00', 5, 'bfarsace_190919_3680_0095.jpg'),
(16, 'TARS', 'TARS è uno dei quattro ex robot tattici del Marine Corps degli Stati Uniti insieme a PLEX, CASE e KIPP presenti nell\'universo interstellare. È uno dei membri dell\'equipaggio di Endurance insieme a Cooper, Brand, Doyle, Romilly e CASE. La personalità di TARS può essere caratterizzata da tratti spiritosi, sarcastici e umoristici, programmati in lui per renderlo un compagno più adatto. TARS sembra anche essere un po\' più versatile di CASE, essendo adatto a compiti che vanno dal pilotaggio alla raccolta dati.\r\n\r\nTARS è doppiato dall\'attore americano Bill Irwin, che ha anche azionato l\'idraulica del robot. Egli controllava il macchinario pesante che pesava quasi 200 libbre. L\'attore è stato rimosso digitalmente durante la post-produzione perché era qualche centimetro più alto del TARS.', '34499.00', 5, '87f40ba43f5ef1f31e14b5622eb2fa76.jpg'),
(17, 'Robot Aspirapolvere', 'Robot Aspirapolvere professionale 4 in 1, Realizzato con due materiali: setole fini che non graffiano il pavimento e non danneggiano i mobili, inoltre è provvisto di silicone in gomma Animal Care, che impedisce che si aggrovigli con i peli di animali domestici come succede in altri robot., Sistema IntelligentCLEAN e 2 potenze d\'aspirazione per multiple superfici , passare il panno e lavare solo riempiendo il suo serbatoio d\'acqua., Il suo sistema WIFI con riconoscimento vocale è compatibile con i sistemi ALEXA e Google Home., Potente turbina con tecnologia Max Power che massimizza la potenza d\'aspirazione fino a 1500 Pa.', '289.00', 1, 'V5s.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `robotincart`
--

CREATE TABLE `robotincart` (
  `id` int(11) NOT NULL,
  `idRobot` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lastOpenNotification` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `idNotification` int(11) NOT NULL,
  `dateNotification` datetime NOT NULL,
  `message` text NOT NULL,
  `idUser` int(11) NOT NULL,
  `idSender` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idAdmin`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD PRIMARY KEY (`idNotification`),
  ADD KEY `fk_idUser` (`idSenderUser`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`idCategory`);

--
-- Indexes for table `robot`
--
ALTER TABLE `robot`
  ADD PRIMARY KEY (`idRobot`),
  ADD KEY `fk_idCategory` (`idCategory`);

--
-- Indexes for table `robotincart`
--
ALTER TABLE `robotincart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_idRobot` (`idRobot`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`idNotification`),
  ADD KEY `fk_idUser` (`idUser`),
  ADD KEY `fk_idSender` (`idSender`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idAdmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_notification`
--
ALTER TABLE `admin_notification`
  MODIFY `idNotification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `idCategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `robot`
--
ALTER TABLE `robot`
  MODIFY `idRobot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `robotincart`
--
ALTER TABLE `robotincart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `idNotification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `robot`
--
ALTER TABLE `robot`
  ADD CONSTRAINT `fk_idCategory` FOREIGN KEY (`idCategory`) REFERENCES `category` (`idCategory`);

--
-- Constraints for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD CONSTRAINT `fk_idSender` FOREIGN KEY (`idSender`) REFERENCES `admin` (`idAdmin`),
  ADD CONSTRAINT `fk_idUser` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
